FROM ubuntu:bionic

RUN apt-get -qq update
RUN apt -qqy upgrade
RUN apt-get -qq update

#install packages
RUN apt-get install -y xvfb curl \
  build-essential xvfb default-jre kmod git ssh-askpass openssh-client \
  python python-openssl zlib1g-dev software-properties-common firefox \
  libgconf2-4 libgtk2.0-0 libgtk-3-0 \
  libnotify-dev libnss3 libxss1 \
  libasound2 libxtst6 \
  ffmpeg sendemail apache2-utils g++ libssl1.0.0 libssl-dev libz-dev \
  wget xauth \
  fonts-liberation libappindicator3-1 libgbm1 xdg-utils

# Trust google signing
RUN curl -sL https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add - && \
  echo "deb http://dl.google.com/linux/chrome/deb/ stable main" > /etc/apt/sources.list.d/google.list

RUN curl -sL https://deb.nodesource.com/setup_12.x | bash -

RUN apt-get -qq update
RUN apt -qqy upgrade

RUN apt-get install -y nodejs openjdk-8-jre

RUN wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
RUN dpkg -i google-chrome-stable_current_amd64.deb


#free up space
RUN rm -rf /var/lib/apt/lists/*

